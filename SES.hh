<?hh //partial
namespace nuclio\plugin\messaging\ses
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	use Aws\Ses\SesClient;
	use Aws\Ses\Exception;

	<<factory>>
	class SES extends Plugin
	{
		private SesClient $client;

		public static function getInstance(/* HH_FIXME[4033] */...$args):SES
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		/**
		 * Creates the Amazon SES client using the given credentials
		 * @param string $awsKey       The amazon key
		 * @param string $awsSecretKey The amazon secret
		 * @param string $region       region for the end point of the Amazon SES API or SMTP interface. valid values are:
		 *                             	REGION:					 |  VALUE:			| URL:								 |	PROTOCOLS:
		 *								US East (N. Virginia)	 |	us-east-1		| email-smtp.us-east-1.amazonaws.com |	SMTP
		 *								US West (Oregon)		 |	us-west-2		| email.us-west-2.amazonaws.com		 |	HTTPS
		 *								US West (Oregon)		 |	us-west-2		| email-smtp.us-west-2.amazonaws.com |	SMTP
		 *								EU (Ireland)			 |	eu-west-1		| email.eu-west-1.amazonaws.com		 |	HTTPS
		 *								EU (Ireland)			 |	eu-west-1		| email-smtp.eu-west-1.amazonaws.com |	SMTP
		 */
		public function __construct(string $awsKey, string $awsSecretKey, string $region, ?string $proxy=null):void
		{
			parent::__construct();
			try
			{
				$config = 
				[
					'credentials' =>
					[
					    'key'      => $awsKey,
					    'secret'   => $awsSecretKey
				    ],
				    'region' => $region
			    ];
			    if(!is_null($proxy))
			    {
			    	$config['request.options'] = ['proxy' => $proxy];
			    }
				$this->client = SesClient::factory($config);	
			}
			catch(Exception $e)
			{
				throw new SESException($e->getMessage());
			}
		}

		/**
		 * Composes an email message based on input data, and then immediately queues the message for sending.
		 * 
		 * @link 	http://docs.aws.amazon.com/ses/latest/APIReference/API_SendEmail.html
		 * @param  	Map $config    	email parametrs:
		 *                          $config = Map
		 *                          {
		 *                          	// Source is required
		 *							    'Source' => 'string',
		 *							    // Destination is required
		 *							    'Destination' => Map
		 *							    {
		 *							    	'ToAddresses' => Vector{'string', ... },
		 *							     	'CcAddresses' => Vector{'string', ... },
		 *							      	'BccAddresses' => Vector{'string', ... },
		 *							    },
		 *							    // Message is required
		 *							    'Message' => Map
		 *							    {
		 *								    // Subject is required
		 *								    'Subject' => Map
		 *								    {
		 *								        // Data is required
		 *								        'Data' => 'string',
		 *								        'Charset' => 'string',
		 *								    },
		 *								    // Body is required
		 *								    'Body' => Map
		 *								    {
		 *							    	    'Text' => Map
		 *							    	    {
		 *							        	    // Data is required
		 *							            	'Data' => 'string',
		 *								            'Charset' => 'string',
		 *								        },
		 *								        'Html' => Map
		 *								        {
		 *								            // Data is required
		 *								            'Data' => 'string',
		 *								            'Charset' => 'string',
		 *								        },
		 *								     },
		 *								 },
		 *								 'ReplyToAddresses' => Vector{'string', ... },
		 *								 'ReturnPath' => 'string',
		 *								 'SourceArn' => 'string',
		 *								 'ReturnPathArn' => 'string',
		 *							 }
		 *
		 *  @return 	string 		  	the messageId	
		 */
		public function send(Map<string,mixed> $config):string
		{
			if(!$config->contains('Source') || !is_string($config->get('Source')))
			{
				throw new SESException("No Source or invalid Source given");
			}
			if(!$config->containsKey('Destination') || is_null($destination = $config->get('Destination')) || !$destination instanceof Map)
			{
				throw new SESException("No Destination or invalid Destination given");
			}
			
			$addresses = $destination->get('ToAddresses');
			if(is_null($addresses) || !$addresses instanceof Vector)
			{
				throw new SESException("Destination field must contain a 'ToAddesses' list.");
			}
			if($addresses->isEmpty())
			{
				throw new SESException("Destination field must contain at least one address in the 'ToAddesses' field.");
			}
			else
			{
				$destination->set('ToAddresses', $addresses->toArray());
				$destination->containsKey('CcAddresses') ? $destination->set('CcAddresses', $destination->get('CcAddresses')->toArray()) : null;
				$destination->containsKey('BccAddresses') ? $destination->set('BccAddresses', $destination->get('BccAddresses')->toArray()) : null;
				$config->set('Destination', $destination->toArray());
			}
			if(!$config->containsKey('Message') || is_null($message = $config->get('Message')))
			{
				throw new SESException("No Message or invalid massage given");
			}
			if(!$message->containsKey('Subject'))
			{
				$message->add(Pair{'Subject', Pair{'Data', ''}});
			}
			else if(!$message->get('Subject')->containsKey('Data'))
			{
				$message->get('Subject')->add(Pair{'Data', ''});
			}
			$message->set('Subject', $message->get('Subject')->toArray());
			if(!$message->containsKey('Body') || is_null($body = $message->get('Body')))
			{
				throw new SESException("No Message body or invalid body given");
			}
			$noText = false;
			$noHtml = false;
			if($body->containsKey('Text') && !is_null($text = $body->get('Text')))
			{
				if(!$text->containsKey('Data'))
				{
					throw new SESException("Text body must contain a data attribute");
				}
				else
				{
					$body->set('Text', $text->toArray());
				}
			}
			else
			{
				$noText = true;
			}
			if($body->containsKey('Html') && !is_null($html = $body->get('Html')))
			{
				if(!$html->containsKey('Data'))
				{
					throw new SESException("Html body must contain a data attribute");
				}
				else
				{
					$body->set('Html', $html->toArray());
				}
			}
			else
			{
				$noHtml = true;
			}
			if($noText && $noHtml)
			{
				throw new SESException("Message body must contain a Text or a Html Key");
			}
			$message->set('Body', $body->toArray());
			$config->set('Message', $message->toArray());

			if($config->containsKey('ReplyToAddresses') && !is_null($replyTo = $config->get('ReplyToAddresses')))
			{
				$config->set('ReplyToAddresses', $replyTo->toArray());
			}

			try
			{
				$result = $this->client->sendEmail($config->toArray());
			}
			catch(Exception $e)
			{
				throw new SESException($e->getMessage());
			}
			if(isset($result['MessageId']))
			{
				return $result['MessageId'];
			}
			else
			{
				throw new SESException("Something went wrong, message not sent. Please try again.");
			}
		}
		/**
		 * Sends an email message, with header and content specified by the client. 
		 * The SendRawEmail action is useful for sending multipart MIME emails. 
		 * The raw text of the message must comply with Internet email standards; otherwise, the message cannot be sent.
		 * @link http://docs.aws.amazon.com/ses/latest/APIReference/API_SendRawEmail.html
		 * @param  Map $config message config
		 * @return mixed ses response
		 */
		public function sendRaw(Map<string,mixed> $config):string
		{
			if(!$config->containsKey('RawMessage') || is_null($message = $config->get('RawMessage')))
			{
				throw new SESException("The RawMessage field is mandatory");
			}
			if(!$message->containsKey('Data'))
			{
				throw new SESException("The RawMessage->Data field is mandatory");
			}
			$config->set('RawMessage', $message->toArray());
			try
			{
				$result = $this->client->sendRawEmail($config->toArray());
			}
			catch(Exception $e)
			{
				throw new SESException($e->getMessage());
			}
			if(isset($result['MessageId']))
			{
				return $result['MessageId'];
			}
			else
			{
				throw new SESException("Something went wrong, message not sent. Please try again.");
			}
		}
		/**
		 * Verifies the given email address as an enabled address for the current AWS account. 
		 * The operations  includes sending a confirmation message to the given address. Verification is only necessary for Source,ReplyToAddress and ReturnPath addresses
		 * @param  string 	$address the email address to verify
		 * @return bool 	true on success, exception on failure
		 */
		public function verifyAddress(string $address):bool
		{
			try
			{
				$result = $this->client->VerifyEmailIdentity(['EmailAddress'=>$address]);
				return true;	
			}
			catch(Exception $e)
			{
				throw new SESException($e->getMessage);
			}
		}
		/**
		 * Removes an email address from the verifeid list
		 * @param  string $address  the address to remove
		 * @return bool      		true on success, exception on failure
		 */
		public function deleteVerifiedAddress(string $address):bool
		{
			try
			{
				$result = $this->client->deleteIdentity(['Identity' => $address]);
				return true;
			}
			catch(Exception $e)
			{
				throw new SESException($e->getMessage());
				
			}
		}
		/**
		 * Returns a list of the verified email addresses
		 * @return Vector list of verified email addresses
		 */
		public function listVerifiedAddresses():Vector
		{
			$result = $this->client->listVerifiedEmailAddresses([]);	
			return new Vector($result['VerifiedEmailAddresses']);
		}
		/**
		 * Adds a domain to the verified identities
		 * @param  string $domain the domain url
		 * @return bool   true on success, exception on fail
		 */
		public function verifyDomain(string $domain):string
		{
			try 
			{
				$result = $this->client->verifyDomainIdentity(['Domain'=>$domain]);	
			} 
			catch (Exception $e) 
			{
				throw new SESException($e->getMessage());
			}
			if(isset($result['VerificationToken']))
			{
				return $result['VerificationToken'];
			}
			else
			{
				throw new SESException("Something went wrong. Please try again");
			}
		}
		/**
		 * Removes a domain from the verified identities
		 * @param  string $domain the domain to remove
		 * @return bool   true on success, exception on fail
		 */
		public function deleteVerifiedDomain(string $domain):bool
		{
			try
			{
				$result = $this->client->deleteIdentity(['Identity' => $domain]);
				return true;
			}
			catch(Exception $e)
			{
				throw new SESException($e->getMessage());
				
			}
		}
		/**
		 * Returs the usage of the email send quota for the current account
		 * @return Map 	the quota usage, with structure:
		 *              
		 *			    Max24HourSend => (int)
		 *			    MaxSendRate => (int)
		 *			    SentLast24Hours => (int)
		 */
		public function getSendQuota():Map<string,string>
		{
			$result = $this->client->getSendQuota([]);
			unset($result['ResponseMetadata']);
			return new Map($result);
		}
		/**
		 * Returns the email send statistics for the current AWS account in the form of SandDataPoints. Each point contains information for a 15 minutes interval.
		 * @return Vector 	List od datapoints. Each DataPoint has the structure:
		 * 
		 *				    Timestamp => (string)
		 *				    DeliveryAttempts => (int)
		 *				    Bounces => (int)
		 *				    Complaints => (int)
		 *				    Rejects => (int)
		 * 
		 */
		public function getSendStatistics():Vector<Map>
		{
			$result = $this->client->getSendStatistics([]);
			if(isset($result['SendDataPoints']))
			{
				$return = Vector{};
				foreach ($result['SendDataPoints'] as $dataPoint)
				{
					$return->add(new Map($dataPoint));
				}
				return $return;
			}
			else
			{
				throw new SESException("Something went wrong. Please try again.");
			}
		}
	}
}